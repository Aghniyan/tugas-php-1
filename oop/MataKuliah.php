<?php

class MataKuliah {
    public $nama;
    public $jumlah_sks = 24;
    public $ruangan = 'Lantai 4';

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    public function get_nama()
    {
        return $this->nama;
    }
    public function get_jumlah_sks()
    {
        return $this->jumlah_sks;
    }
    public function get_ruangan()
    {
        return $this->ruangan;
    }
}
?>