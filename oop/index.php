<?php
require('MataKuliah.php');
require('Indonesia.php');
require('Fisika.php');

$mtk = new MataKuliah("Matematika");
echo "Nama : ". $mtk->get_nama()."<br>";
echo "Jumlah SKS : ".$mtk->get_jumlah_sks()."<br>";
echo "Ruangan : ".$mtk->get_ruangan()."<br>";

echo "<br>";
$indo = new Indonesia("Bahasa Indonesia");
echo "Nama : ". $indo->get_nama()."<br>";
echo "Jumlah SKS : ".$indo->get_jumlah_sks()."<br>";
echo "Ruangan : ".$indo->get_ruangan()."<br>";
echo "Ulangan : ". $indo->ulangan();
echo "<br>";
echo "<br>";

$fisika = new Fisika("Fisika");
echo "Nama : ". $fisika->get_nama()."<br>";
echo "Jumlah SKS : ".$fisika->get_jumlah_sks()."<br>";
echo "Ruangan : ".$fisika->get_ruangan()."<br>";
echo "Praktek : ". $fisika->praktek();

?>